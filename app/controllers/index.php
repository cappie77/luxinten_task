<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:25
 */
namespace app\controllers;
use app\core\controller;


class Index extends Controller {

    public function news()
    {
        return $this->renderTemplate('news/index' );
    }
    public function add()
    {
        return $this->renderTemplate('news/add' );
    }
    public function delete()
    {
        return $this->renderTemplate('news/delete' );
    }
    public function edit()
    {
        return $this->renderTemplate('news/edit' );
    }
}