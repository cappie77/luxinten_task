<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:26
 */
namespace app\core;

class response
{
    /**
     * 404
     */
    public static function error404()
    {
        self::applyHeader('HTTP/1.0 404 Not Found',404);
    }
    /**
     * 503
     */
    public static function error503()
    {
        self::applyHeader('HTTP/1.1 503 Service Temporarily Unavailable');
        self::applyHeader('Status:503 Service Temporarily Unavailabl');
        self::applyHeader('Retry-After:300');
    }
    /**
     * 400
     */
    public static function error400()
    {
        self::applyHeader('HTTP/1.1 400 BAD REQUEST',400);
    }
    private static function applyHeader(string $header,int $code=null,bool $replace=true):bool
    {
    if(\headers_sent()) {
    return false;
     }
      \header($header,$replace,$code);
      return true;
    }
}
