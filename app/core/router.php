<?php

namespace app\core;
use app\core\factory;
use app\core\response;

class router
{
    private  $controller ='Index';
    private  $action='index';
    private  $route='';
    private  $reg_paths= array(
        '/\/([a-z0-9+_\-]+)\/([a-z0-9+_\-]+)/' => 'controller/action',
        '/^\/([a-z0-9+_\-]+$)/' => 'controller',
    );
    private  static  $out;
    public  static  function  create(string $method,string $route)
    {
        return new self($method,$route);
    }
    public  static  function  getOut()
    {
        return self::$out;
    }
    private function  __construct(string $method,string $route)
    {
        $this->method=$method;
        $this->route=$route;
        $this->run();
    }
    private  function  run()
    {
        $this->parse($this->route);
        $controllers=self::getControllers();
        if (empty($controllers)) {
            throw new \Exception('There are no controllers');
        }
        if(!$this->getMatchController($controllers,$this->controller)){
            response::error404();
            return false;
        }
        if(!$this->applyController($this->controller)){
            throw new \Exception('Controller file was not found');
        }
        $namespace=$this->getControllerNamespace($this->controller);
        if(!\is_callable(array($namespace,$this->action))){
        response::error404();
        return false;
    }
        $controller=factory::create($namespace);
        static::$out=$this->call([$controller,$this->action],[]);
        return true;
    }
    private  function  call($callback=[],$args=[])
    {
        return \call_user_func_array($callback,$args);
    }


    private  function prepareForNs(string $what): string
    {
         return "app\\controllers\\".ucfirst($what);
    }
    private function prepareForPath(string $controllerName):string
    {
        return \str_replace('\\','/',$controllerName);
    }
    private function getMatchController($controllers,$currentController):bool
    {
        return in_array($currentController,$controllers);
    }
    private function applyController(string $controllerName):bool
    {
        $fullControllerName = $this->prepareForPath($controllerName) . \PHP_EXTENSION;
        $path = CONTROLLER_DIR . DS . $fullControllerName;
        if (!\file_exists($path)) {
            throw new \Exception('Controller path not found');
        }
        include_once $path;
        return true;
    }
private function getControllerNamespace(string $controller): string
    {
        $namespace = $this->prepareForNs($controller);
        return $namespace;
    }

private static function getControllers(): array
    {
        if (\file_exists(CONTROLLER_DIR)) {
            $result = \scandir(CONTROLLER_DIR);
            if ($result) {
                return array_reduce($result, function ($acc, $item) {
                    $item = pathinfo($item, PATHINFO_FILENAME);
                    list($name) = explode('.', $item);
                    if (!empty($name)) {
                        $acc[] = $name;
                    }
                    return $acc;
                }, []);
            }
        } else {
            throw new \Exception('Controllers folder was not found');
        }
    }
function parse($path) {
    foreach ($this->reg_paths as $regxp => $keys)
        if (preg_match_all($regxp , $path, $res)) {
               $keys = explode('/', $keys);
            foreach ($keys as $i => $key) {
                list($value) = $res[$i + 1];
                $this->$key = $value; }
        }
}

}