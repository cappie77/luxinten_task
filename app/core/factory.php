<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:27
 */
namespace app\core;
class factory
{
    /**
 * @param $type
 * @param array $params
 * @return mixed
 */
    public static function create($type,$params=[])
    {
        $some=new $type($params);
        return $some;
    }
}
