<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:27
 */
namespace app\core;

class request
{
    public  static  function  Get(string $key='')
    {
        if(empty($key)){
            return $_GET;
        }
        return self::getProperty($_GET,$key);
    }
    public static function Post(string $key = '') {
        if (empty($key)) {
            return $_POST;
        }
        return self::getProperty($_POST, $key);
    }
    public static function hasPost() {
        return !empty($_POST);
    }
    public static function hasGet() {
        return !empty($_GET);
    }
    public  static  function  getProperty($data,$key,$default=null)
    {
        if (\is_array($data) && isset($data[$key])) {
            return $data[$key];
        }
        if (\is_object($data) && isset($data->{$key})) {
            return $data->{$key};
        }
        return $default;
    }
}