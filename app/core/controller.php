<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:27
 */

namespace app\core;


abstract class controller
{
    protected $db;

    public function __construct()
    {
        $this->db = new \app\Db;
    }

    public function paginglink()
    {
        $query = "SELECT * FROM news";
        $records_per_page = 3;
        $self = '/index/news';
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $total_no_of_records = $stmt->rowCount();
        if ($total_no_of_records > 0) {
            ?>
            <ul class="pagination"><?
            $total_no_of_pages = ceil($total_no_of_records / $records_per_page);
            $current_page = 1;
            if (isset($_GET["page_no"])) {
                $current_page = $_GET["page_no"];
            }
            if ($current_page != 1) {
                $previous = $current_page - 1;
                echo "<li><a href='" . $self . "?page_no=1'>Перша</a></li>";
                echo "<li><a href='" . $self . "?page_no=" . $previous . "'>Попередня</a></li>";
            }
            for ($i = 1; $i <= $total_no_of_pages; $i++) {
                if ($i == $current_page) {
                    echo "<li><a href='" . $self . "?page_no=" . $i . "'style='color:red;'>" . $i . "</a></li>";
                } else {
                    echo "<li><a href='" . $self . "?page_no=" . $i . "'>" . $i . "</a></li>";
                }
            }
            if ($current_page != $total_no_of_pages) {
                $next = $current_page + 1;
                echo "<li><a href='" . $self . "?page_no=" . $next . "'>Наступна</a></li>";
                echo "<li><a href='" . $self . "?page_no=" . $total_no_of_pages . "'>Остання</a></li>";
            }
            ?></ul><?
        }
    }

    public function paging($query, $records_per_page, $page_num)
    {
        $starting_position = 0;
        if (isset($page_num)) {
            $starting_position = ($page_num - 1) * $records_per_page;
        }
        $query2 = $query . " limit $starting_position,$records_per_page";
        return $query2;
    }

    public function select($page_num)
    {
        $query = "SELECT * FROM news";
        $records_per_page = 3;
        $newquery = $this->paging($query, $records_per_page, $page_num);
        $stmt = $this->db->prepare($newquery);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function create($ntitle, $ntext, $nautor, $ndate)
    {
        try {
            $stmt = $this->db->prepare("INSERT INTO news(ntitle, ntext, nautor, ndate) VALUES(:ntitle, :ntext, :nautor, :ndate)");
            $stmt->bindParam(":ntitle", $ntitle);
            $stmt->bindParam(":ntext", $ntext);
            $stmt->bindParam(":nautor", $nautor);
            $stmt->bindParam(":ndate", $ndate);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function update($nid, $ntitle, $ntext, $nautor, $ndate)
    {
        try {
            $stmt = $this->db->prepare("UPDATE news SET ntitle=:ntitle, ntext=:ntext, nautor=:nautor, ndate=:ndate WHERE nid=:nid");
            $stmt->bindparam(":ntitle", $ntitle);
            $stmt->bindparam(":ntext", $ntext);
            $stmt->bindparam(":nautor", $nautor);
            $stmt->bindparam(":ndate", $ndate);
            $stmt->bindparam(":nid", $nid);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getID($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM news WHERE nid=:nid");
        $stmt->execute(array(":nid" => $id));
        $editRow = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $editRow;
    }

    public function delete_news($id)
    {
        $stmt = $this->db->prepare("DELETE FROM news WHERE nid=:nid");
        $stmt->bindparam(":nid", $id);
        $stmt->execute();
        return true;
    }

    public function renderTemplate(string $viewName)
    {
        $filePath = \realpath(TEMPLATE_DIR . DS . $viewName . \TEMPLATE_EXTENSION);
        if (!\file_exists($filePath)) {
            throw new \Exception($filePath . ' template file is not exists');
        }
        require_once($filePath);
        return $this;
    }
}