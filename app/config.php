<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:24
*/
define('DS',DIRECTORY_SEPARATOR);
define('APP_DIR',realpath('app'));
define('CONTROLLER_DIR',realpath('app/controllers'));
define('TEMPLATE_DIR',realpath('app/templates'));
define('CONFIG_PATH',realpath('config'));
define('PHP_EXTENSION','.php');
define('TEMPLATE_EXTENSION','.phtml');
require_once 'application.php';
require_once 'dbconfig.php';
