<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 31.03.2018
 * Time: 15:24
 */

namespace app;
use app\core\response;
use app\core\router;
class application
{
    public static function run() {
        return new self();
    }
    private function __construct() {

        \spl_autoload_register([$this, 'loader']);
        return $this->getRoute();
    }
    private function loader(string $file, bool $dir = false)
    {
        $file = \str_replace('\\', '/', $file);
        $filePath = $this->getPaths($file, $dir);
        if (\file_exists($filePath)) {
            require_once($filePath);
            return '';
        }
        return $filePath;
    }
    private function getPaths(string $file, bool $dir): string {
        $filePath = $file . \PHP_EXTENSION;
return $filePath;
        }
private function getRoute() {
    try {
        $router = self::getRouter();
        return $router->getOut();

    } catch (Exception $e) {
         Response::error503();
        return false;
    }
}
private static function getRouter() {
    return Router::create($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
}


}