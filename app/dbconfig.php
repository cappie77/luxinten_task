<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 01.04.2018
 * Time: 17:37
 */
namespace app;
class Db extends \PDO
{
    protected $hostname = "localhost";
    protected $password = "";
    protected $user = "root";
    protected $name = "crud_news";
    public function __construct()
    {
        $aDriverOptions[\PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES UTF8';
        parent::__construct('mysql:host=' . $this->hostname . ';dbname=' .  $this->name . ';', $this->user, $this->password, $aDriverOptions);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
}
