-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Квт 08 2018 р., 14:38
-- Версія сервера: 5.6.38-log
-- Версія PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `crud_news`
--

-- --------------------------------------------------------

--
-- Структура таблиці `news`
--

CREATE TABLE `news` (
  `nid` int(11) NOT NULL,
  `ntitle` varchar(255) NOT NULL,
  `ntext` text NOT NULL,
  `nautor` varchar(255) NOT NULL,
  `ndate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `news`
--

INSERT INTO `news` (`nid`, `ntitle`, `ntext`, `nautor`, `ndate`) VALUES
(1, 'Test', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test про', 'admin', '2018-04-07'),
(2, 'Новина 22bcvb', 'авіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срапавіпваіпвіа  срап', 'Адмін', '2018-04-07'),
(5, 'dgfhfg', 'dfghddfdfghddfdfghddfdfghddfdfgh\r\nddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdf\r\nghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddfdfghddf', 'fghfdghfg', '2018-04-22'),
(6, 'ваіпвіа', 'півапвапвіапароплршнгшн', 'віапвіапві', '2018-04-12'),
(7, 'sdfsdfsd', 'sdfsdfsdfdfsdf sdfsdf sdfsdfsdfsd sdfsdfssdfsdfsdfdfsdf sdfsdf sdfsdfsdfsd \r\nsdfsdfsdfdfsdf sdfsdf sdfsdfsdfsd sdfsdfsdfdfsdf sdfsdf sdfsdfsdfsd \r\ndfdfsdf sdfsdf sdfsdfsdfsd sdfsdfsdfdfsdf sdfsdf sdfsdfsdfsd ', 'fdsf', '2018-04-05');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`nid`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `news`
--
ALTER TABLE `news`
  MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
